﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeUpHelper.Constants
{
    class WebPath
    {
        public const string TradeUpHelperSite = "https://tradeuphelper-csgo.site/";

        public static string CSGOFloat = "https://api.csgofloat.com/?url=";
        public static string Steam = "https://steamcommunity.com";
        public static string SteamMarket = "https://www.steamcommunity.com/market/search?q=";
        public static string LootFarmPriceDBFile = "https://loot.farm/fullprice.json";
        public static string Cyber_sports_ru = "https://cyber.sports.ru/cs/news/";
        public const string UPDATES_ICONS_PATH = "https://tradeuphelper-csgo.site/TradeUpHelperProgram/UpdatesIcons/";
        public const string UPDATE_CHANGE_LOG_PATH = "https://tradeuphelper-csgo.site/TradeUpHelperProgram/LastUpdateLog.xml";

    }
}
