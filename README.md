![logoBanner](https://user-images.githubusercontent.com/45515206/133918104-640d1d79-763f-4579-a2ac-b7b9f1e0bfae.png)

# TradeUp_Helper
Программа для подсчета износа скина после контракта CS GO, сохранения истории крафтов, поиска редких паттернов или наклеек на торговой площадке Steam

A program for calculating skin wear after a CS GO contract, saving crafting history, searching for rare patterns or stickers on the Steam marketplace

DEMO

![MenuPurple](https://user-images.githubusercontent.com/45515206/140391296-f2a8258d-bf25-4eef-abd7-d4da5ba28687.PNG)
![MenuBlack](https://user-images.githubusercontent.com/45515206/140391320-a399531f-cdec-4e94-b0ab-adf798ddf033.PNG)
![MarketChecker](https://user-images.githubusercontent.com/45515206/140391337-710cc944-c14b-42a6-8145-cdb82b6e832f.PNG)
![TradeCalc](https://user-images.githubusercontent.com/45515206/140391348-66ccd199-1a06-47fc-bce2-a939978c3bc5.PNG)
